package com.testCases;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.pageObjects.LoginPage;

public class TC_LoginTest_01 extends BaseClass
{
	@Test
	public void loginTest()
	{
	
		logger.info("URL is opened");
		
		LoginPage lp=new LoginPage(driver);
		
		lp.setUserName(username);
		logger.info("Entered username");
		
		lp.setPassword(password);
		logger.info("Entered password");
		
		lp.ClickSubmit();
		
		String str=driver.getTitle();
		System.out.println("Title is ="+str);
		
		if(driver.getTitle().equals("GTPL Bank Manager HomePage"))
		{
			Assert.assertTrue(true);
			logger.info("Login successfull");
		
		}
		else
		{
			Assert.assertTrue(false);
			logger.info("Login fail");
		}
		
	}

}
