package com.testCases;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.utilities.ReadConfig;

public class BaseClass {
	
	ReadConfig rc=new ReadConfig();

	public String baseURL=rc.getApplicationURL();
	public String username=rc.getUsername();
	public String password=rc.getPassword();
	public static WebDriver driver;
	public static Logger logger;
	
	@Parameters("Browser")
	@BeforeClass
	
	public void setup(String br)
	{
		
		logger=Logger.getLogger("Project-1");
		PropertyConfigurator.configure("Log4j.properties");
		
		//E:\MyWorkSpace\Selenium_Projects\Drivers\chromedriver.exe
		
/*		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//Drivers//chromedriver.exe");
		driver =new ChromeDriver();
	
		
		System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"//Drivers//IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",rc.getIEPath());
		driver =new InternetExplorerDriver();
		
		System.setProperty("webdriver.gecko.driver",rc.getFirFoxPath());
		driver = new FirefoxDriver();
		
	*/	
		if(br.equals("chrome"))
		{
		System.setProperty("webdriver.chrome.driver",rc.getchromePath());
		driver =new ChromeDriver();
		}
		else if(br.equals("ie"))
		{
			System.setProperty("webdriver.ie.driver",rc.getIEPath());
			driver =new InternetExplorerDriver();
			
		}
		
		else if(br.equals("firfox"))
		{
			System.setProperty("webdriver.gecko.driver",rc.getFirFoxPath());
			driver = new FirefoxDriver();
		}
		driver.get(baseURL);
	}
	
	
	@AfterClass
	public void tearDown()
	{
		driver.quit();
	}
	
	
}
